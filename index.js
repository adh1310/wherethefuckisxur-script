const request = require('request-promise-native');
const fs = require('fs');
const Discord = require('discord.js');
const markdown = require('markdown').markdown;

const client = new Discord.Client();

let msgData;
if (fs.existsSync('storage/msg.json')) {
    msgData = JSON.parse(fs.readFileSync('storage/msg.json'));
} else {
    msgData = {
        xurmsg: ''
    }
    fs.writeFileSync('storage/msg.json', JSON.stringify(msgData, undefined, 2));
}

var manifest;
var manifesturl;

const morningReset = 17;
const afternoonReset = 0;
const xurDay = 5;
const weeklyReset = 2;

const channel = '518150211357507588';
const guild = '494890625330577409';

var xurFile;
var msgFile;
var vendorFile;
var cyclesFile;

let startTime = new Date();
if (!fs.existsSync('storage/xur.json')) {
    if (!isItXurTime()) {
        xurFile = {
            present: false
        };
        fs.writeFileSync('storage/xur.json', JSON.stringify(xurFile, undefined, 2));
    } else {
        xurFile = {
            present: true,
            found: false
        };
        fs.writeFileSync('storage/xur.json', JSON.stringify(xurFile, undefined, 2));
    }
} else {
    xurFile = JSON.parse(fs.readFileSync('storage/xur.json', 'utf8'));
}

if (!fs.existsSync('storage/config.json')) {
    throw "No config.json, can't run";
}

let config = JSON.parse(fs.readFileSync('storage/config.json'));

if (config.token.expires < (new Date).getTime() / 1000) {
    throw "Refresh token expired, please get a new one!";
}

client.login(config.discordbot).then(() => {
    queueXur();
});

client.on('message', message => {
    let args = message.content.split(' ');
    if (args[0][0] == '$') {
        let author = message.guild.members.get(message.author.id);
        if (author.hasPermission('VIEW_AUDIT_LOG')) {
            if (args[0] == '$xur') {
                if (!isItXurTime()) {
                    message.reply('xur isn\'t here so you can\'t update his location!');
                } else {
                    let xur = {};
                    switch (args[1].toLowerCase()) {
                        case 'tower':
                            xur.planet = 'Tower';
                            xur.zone = 'The Hangar';
                            break;
                        case 'titan':
                            xur.planet = 'Titan';
                            xur.zone = 'The Rig';
                            break;
                        case 'io':
                            xur.planet = 'Io';
                            xur.zone = 'Giant\'s Scar';
                            break;
                        case 'nessus':
                            xur.planet = 'Nessus';
                            xur.zone = 'Watcher\'s Grave';
                            break;
                        case 'earth':
                        case 'edz':
                            xur.planet = 'Earth';
                            xur.zone = 'Winding Cove';
                            break;
                        case 'mars':
                            xur.planet = 'Mars';
                            xur.zone = 'Somewhere';
                            break;
                        case 'mercury':
                            xur.planet = 'Mercury';
                            xur.zone = 'Somewhere';
                            break;
                        case 'shore':
                        case 'tangled shore':
                        case 'tangledshore':
                            xur.planet = 'Tangled Shore';
                            xur.zone = 'Somewhere';
                            break;
                        case 'city':
                        case 'dreaming city':
                            xur.planet = 'Dreaming City';
                            xur.zone = 'Somewhere';
                            break;
                        case 'moon':
                            xur.planet = 'Moon';
                            xur.zone = 'Somewhere';
                            break;
                    }
                    if (xur == {}) {
                        message.reply('that\'s not a valid location, please use `tower`, `titan`, `io`, `nessus`, `earth`, `mercury`, `mars`, `shore`, `city`, or `moon`!');
                    } else {
                        xur.found = true;
                        xur.present = true;
                        xur.lastUpdate = new Date();
                        if (xur != xurFile) {
                            xurFile = Object.assign({}, xur);
                            fs.writeFileSync('storage/xur.json', JSON.stringify(xur, undefined, 2));
                        }
                        message.reply('updated!');
                    }
                }
            } else if (args[0] == "$xurmsg") {
                let newContent = args.slice(1, args.length).join(' ');
                newContent = markdown.toHTML(newContent);
                msgData.xurmsg = newContent;
                if (msgData != msgFile) {
                    msgFile = Object.assign({}, msgData);
                    fs.writeFileSync('storage/msg.json', JSON.stringify(msgData, undefined, 2));
                }
                message.reply('updated the xurmsg to say ```' + newContent + '```');
            } else if (args[0] == "$psa") {
                let newContent = args.slice(1, args.length).join(' ');
                newContent = markdown.toHTML(newContent);
                msgData.psa = newContent;
                if (msgData != msgFile) {
                    msgFile = Object.assign({}, msgData);
                    fs.writeFileSync('storage/msg.json', JSON.stringify(msgData, undefined, 2));
                }
                message.reply('updated the psa to say ```' + newContent + '```');
            } else if (args[0] == "$riff") {
                let newContent = args.slice(1, args.length).join(' ');
                newContent = markdown.toHTML(newContent);
                msgData.riff = newContent;
                if (msgData != msgFile) {
                    msgFile = Object.assign({}, msgData);
                    fs.writeFileSync('storage/msg.json', JSON.stringify(msgData, undefined, 2));
                }
                message.reply('updated the riff to say ```' + newContent + '```');
            } else {
                message.reply('command not found');
            }
        } else {
            message.reply('you must be a moderator to use this command!');
        }
    }
})



runFetch();

function runFetch() {
    console.log('running bungiefetch');
    bungiefetch().then(() => {
        setTimeout(runFetch, 60000);
    }, (error) => {
        console.log(error);
        setTimeout(runFetch, 60000);
    })
}

async function bungiefetch() {
    let time = new Date();
    if (!isItXurTime()) {
        if (xurFile.present == true) {
            xurFile = {
                present: false
            }
            fs.writeFileSync('storage/xur.json', JSON.stringify(xurFile, undefined, 2));
        }
    } else {
        if (xurFile.present == false) {
            xurFile = {
                present: true,
                found: false
            }
            fs.writeFileSync('storage/xur.json', JSON.stringify(xurFile, undefined, 2));
        }
    }

    let refreshurl = 'https://www.bungie.net/platform/app/oauth/token/';
    let refreshopts = {
        form: {
            'grant_type': 'refresh_token',
            'refresh_token': config.token.refresh,
            'client_id': config.api.id,
            'client_secret': config.api.secret
        },
        json: true
    };
    let refresh = await request.post(refreshurl, refreshopts);
    let token = refresh.access_token;
    config.token.refresh = refresh.refresh_token;
    config.token.expires = ((new Date).getTime() / 1000) + 7776000;

    fs.writeFileSync('storage/config.json', JSON.stringify(config, undefined, 2));

    let headers = {
        'X-API-Key': config.api.key,
        'Authorization': 'Bearer ' + token
    };

    let manifestsurl = 'https://www.bungie.net/platform/Destiny2/Manifest';
    let manifestsopts = {
        headers: headers,
        json: true,
        gzip: true
    }
    let manifests = await request.get(manifestsurl, manifestsopts);

    let newmanifesturl = 'https://www.bungie.net' + manifests.Response.jsonWorldContentPaths.en;
    if (newmanifesturl != manifesturl) {
        manifesturl = newmanifesturl;
        let manifestopts = {
            headers: headers,
            json: true,
            gzip: true
        }
        manifest = await request.get(manifesturl, manifestopts);
    }

    let cycles = {};

    cycles.activenightfalls = [];
    // let nightfallurl = 'https://www.bungie.net/platform/Destiny2/Milestones';
    // let nightfallopts = {
    //     headers: headers,
    //     json: true
    // }
    // let nightfallresp = await request.get(nightfallurl, nightfallopts);
    // let nightfallactivities = nightfallresp.Response['2171429505'].activities;
    // nightfallactivities.forEach((activity) => {
    //     let activitydef = manifest.DestinyActivityDefinition[activity['activityHash']];
    //     // console.log(activitydef);
    //     if ('modifierHashes' in activity) {
    //         let nightfallname = activitydef.displayProperties.name;
    //         nightfallname = nightfallname.substr(nightfallname.indexOf(" ") + 1);
    //         cycles.activenightfalls.push(nightfallname);
    //     }
    // });

    let nightfallparams = {
        'components': '204'
    }
    let nightfallurl = 'https://www.bungie.net/platform/Destiny2/' + config.charinfo.platform + '/Profile/' + config.charinfo.membershipid + '/Character/' + config.charinfo.charid;
    let nightfallopts = {
        headers: headers,
        qs: nightfallparams,
        json: true,
    }
    try {
        let nightfallresp = await request.get(nightfallurl, nightfallopts);

        nightfallresp.Response.activities.data.availableActivities.forEach((activity) => {
            let activityDef = manifest.DestinyActivityDefinition[activity.activityHash];
            if (activityDef.activityTypeHash == 575572995 && activityDef.modifiers.length > 0 && activityDef.activityLevel == 50 && activityDef.activityLightLevel <= 820) {
                if (activityDef.activityLightLevel == 750) {
                    cycles.activeordeal = activityDef.displayProperties.description
                } else {
                    let nightfallname = activityDef.displayProperties.name;
                    nightfallname = nightfallname.substr(nightfallname.indexOf(" ") + 1);
                    cycles.activenightfalls.push(nightfallname);
                }
            }
        })
    } catch (e) {
        console.log(e);
    }

    let firstResetTime = 1539709200;
    let currentTime = Math.floor((new Date()).getTime() / 1000);
    let secondsSinceFirst = currentTime - firstResetTime;
    let daysSinceFirst = Math.floor(secondsSinceFirst / 86400);
    let weeksSinceFirst = Math.floor(secondsSinceFirst / 604800);

    let dailies = ['Crucible', 'Heroic Adventure', 'Strikes', 'Gambit'];
    let challenges = ['Ouroborea', 'Forfeit Shrine', 'Shattered Ruins', 'Keep of Honed Edges', 'Agonarch Abyss', 'Cimmerian Garrison'];
    let challengeLocs = ['Aphelion\'s Rest > floating around in the main area', 'Gardens of Esila > off the cliff near the entrance', 'Spine of Keres > above a rock in the purple fog area', 'Harbinger\'s Seclude > on top of the statue in back of the area', 'Bay of Drowned Wishes > in the bay area, just off the main path', 'Chamber of Starlight > on the cliff behind the chest']
    let epbosses = ['Naksud, the Famine', 'Bok Litur, Hunger of Xol', 'Nur Abath, Crest of Xol', 'Kathok, Roar of Xol', 'Damkath, the Mask'];
    let epguns = ['Every IKELOS gun', 'Every IKELOS gun', 'IKELOS_SG_v1.0.1 (Shotgun)', 'IKELOS_SMG_v1.0.0 (SMG)', 'IKELOS_SR_v1.0.1 (Sniper)'];
    let wellbosses = ['Sikariis and Varkuuriis, Plagues of the Well', 'Cragur, Plague of the Well', 'Inomia, Plague of the Well'];
    let citystatuses = ['Weakest', 'Growing', 'Strongest'];
    let reckoningbosses = ['Knights', 'Oryx'];

    cycles.dailies = {
        current: dailies[daysSinceFirst % 4],
        next: [dailies[(daysSinceFirst + 1) % 4], dailies[(daysSinceFirst + 2) % 4], dailies[(daysSinceFirst + 3) % 4]]
    }

    cycles.ascendantchallenge = {
        name: challenges[weeksSinceFirst % 6],
        location: challengeLocs[weeksSinceFirst % 6],
        id: weeksSinceFirst % 6
    }
    cycles.escalationprotocol = {
        boss: epbosses[weeksSinceFirst % 5],
        gun: epguns[weeksSinceFirst % 5],
        id: weeksSinceFirst % 5
    }
    cycles.citystatus = {
        status: citystatuses[(weeksSinceFirst + 2) % 3],
        boss: wellbosses[(weeksSinceFirst + 2) % 3],
        id: (weeksSinceFirst + 2) % 3
    }

    cycles.reckoningbosses = {
        boss: reckoningbosses[weeksSinceFirst % 2]
    }

    if (cyclesFile != cycles) {
        cyclesFile = cycles;
        fs.writeFileSync('storage/cycles.json', JSON.stringify(cycles, undefined, 2));
    }


    let vendorparams = {
        'components': '400,401,402,300,301,302,303,304,305,306,307,308'
    }

    let vendorurl = 'https://www.bungie.net/platform/Destiny2/' + config.charinfo.platform + '/Profile/' + config.charinfo.membershipid + '/Character/' + config.charinfo.charid + '/Vendors';
    let vendoropts = {
        headers: headers,
        qs: vendorparams,
        json: true,
        gzip: true
    }

    let vendorsavedata = {};

    try {
        let vendordata = await request.get(vendorurl, vendoropts);
        if (!('Response' in vendordata)) {
            throw 'BungieDown';
        }
        let vendorsales = vendordata.Response.sales.data;
        let vendorcats = vendordata.Response.categories.data;
        let vendoritems = vendordata.Response.itemComponents;

        let vendorcatstoget = {
            '3982706173': [9], //Asher Mir
            '3603221665': [14, 15], //Lord Shaxx
            '3361454721': [21, 24], //Tess Everis
            '3347378076': [5, 6], //Suraya Hawthorne
            '2917531897': [3, 4, 2], //Ada-1
            '2398407866': [10], //Brother Vance
            '1735426333': [8], //Ana Bray
            '1576276905': [8], //Failsafe
            // '1265988377': [5], //Benedict 99-40
            '1062861569': [9], //Sloane
            '997622907': [1], //Prismatic Matrix
            // '919809084': [0, 1, 4], //Eva Levante
            '863940356': [1, 4, 5], //Spider
            '672118013': [4], //Banshee-44
            '396892126': [8], //Devrim Kay
            '248695599': [6, 7], //The Drifter
            '69482069': [13], //Commander Zavala
            // '895295461': [6, 7], //Lord Saladin
            '2190858386': [1], //Xur,
            '1841717884': [3, 4] //Petra Venj
        }

        for (let key in vendorcatstoget) {
            if (key in vendoritems) {
                let allitemsockets = vendoritems[key].sockets.data;

                let cats = vendorcats[key].categories;
                let vendorcatsdata = []
                for (let cattogetkey in vendorcatstoget[key]) {
                    let cattoget = vendorcatstoget[key][cattogetkey];
                    let cat;
                    for (let vendorcatkey in cats) {
                        let vendorcat = cats[vendorcatkey];
                        if (vendorcat.displayCategoryIndex == cattoget) {
                            cat = vendorcat;
                        }
                    }
                    if (cat) {
                        let catdata = {
                            display: manifest.DestinyVendorDefinition[key].displayCategories[cat.displayCategoryIndex].displayProperties,
                            items: []
                        }
                        for (let itemkey in cat.itemIndexes) {
                            let itemindex = cat.itemIndexes[itemkey];
                            let saleitem = vendorsales[key].saleItems[itemindex];
                            let itemhash = saleitem.itemHash;

                            catdata.items.push(buildItemData(itemhash, saleitem, allitemsockets));
                        }
                        vendorcatsdata.push(catdata);
                    }
                }
                if (vendorcatsdata.length > 0) {
                    vendorsavedata[key] = {
                        display: manifest.DestinyVendorDefinition[key].displayProperties,
                        categories: vendorcatsdata
                    }
                }
            }
        }
    } catch (e) {
        console.log(e);
    }
    if (vendorFile != vendorsavedata) {
        vendorFile = vendorsavedata;
        fs.writeFileSync('storage/vendor.json', JSON.stringify(vendorsavedata, undefined, 2));
    }
    console.log('done!');
}

function queueNext(nextRun) {
    setTimeout(() => {
        runFetch();
    }, nextRun)
}

async function doXur() {
    console.log('doing xur');
    let chan = client.guilds.get(guild).channels.get(channel);
    let message = await chan.send('<@&518149972340768768>\nIt\s almost Xur time!  Go ahead and react to the planet you\'ll be checking, try to make sure there\'s none left over!  Also wait until reset has happened in approximately 5 minutes before loading into the area you\'re checking!\n\n:one:: Tower > The Hangar > Above Dead Orbit\n:two:: Titan > The Rig > In a control room\n:three:: Io > Giant\'s Scar > In a cave\n:four:: Nessus > Watcher\'s Grave > In the tree\n:five:: Earth (EDZ) > Winding Cove > On the cliff\nThe other planets are listed in the reactions, but we don\'t know where he is on them.  They\'re there as preparation, so you don\'t have to sign up for them yet.');
    await message.react(message.guild.emojis.get('520637103776989195'));
    await message.react(message.guild.emojis.get('520634668580864010'));
    await message.react(message.guild.emojis.get('520637103420211202'));
    await message.react(message.guild.emojis.get('520637103285993473'));
    await message.react(message.guild.emojis.get('520637103927853059'));
    await message.react(message.guild.emojis.get('619969187946954766'));
    await message.react(message.guild.emojis.get('619970511530885120'));
    await message.react(message.guild.emojis.get('619971213107658771'));
    await message.react(message.guild.emojis.get('619971992971640832'));
    await message.react(message.guild.emojis.get('619984459487379476'));

    let time = new Date();
    let reset = new Date(Date.UTC(time.getUTCFullYear(), time.getUTCMonth(), time.getUTCDate(), morningReset));
    let timeToReset = reset - time;
    await delay(timeToReset);
    await chan.send('<@&518149972340768768>\nGOGOGOGOGOGOGOGO');
    // setTimeout(() => {
    //     chan.send('Xur\'s here!  Go out and check your spots, and if you spot him react to this message with the corresponding number to the location you went to!').then(newmsg => {
    //         newmsg.react('\uFE0F').then(() => {
    //             newmsg.react('2').then(() => {
    //                 newmsg.react('3').then(() => {
    //                     newmsg.react('4').then(() => {
    //                         newmsg.react('5').then(() => {

    //                         })
    //                     })
    //                 })
    //             })
    //         })
    //     })
    // }, 1000)
}

function getMsToXur() {
    let currentTime = new Date();
    let daysToXurDay = ((xurDay + 7) - currentTime.getUTCDay()) % 7; // first num should be 12, changed for testing
    if (daysToXurDay == 0) {
        if (currentTime.getUTCHours() == morningReset - 1) { //should be 16, changing for testing
            if (currentTime.getUTCMinutes() >= 50) {
                daysToXurDay = 7;
            }
        } else if (currentTime.getUTCHours() > morningReset - 1) {
            daysToXurDay = 7;
        }
    }
    let nextXur = new Date(Date.UTC(currentTime.getUTCFullYear(), currentTime.getUTCMonth(), currentTime.getUTCDate() + daysToXurDay, morningReset - 1, 50));
    return nextXur - currentTime;
}

function queueXur() {
    setTimeout(() => {
        doXur().then(() => {
            queueXur();
        })
    }, getMsToXur());
}

function buildItemData(itemhash, saleitem, itemsockets) {
    let itemdef = manifest.DestinyInventoryItemDefinition[itemhash];
    let itemdisplay = itemdef.displayProperties;
    let item = {
        display: itemdisplay,
        costs: [],
    }

    //Find price currency and quantity
    for (let costkey in saleitem.costs) {
        let cost = saleitem.costs[costkey];
        let costdef = manifest.DestinyInventoryItemDefinition[cost.itemHash];
        let costsave = {
            amount: cost.quantity,
            display: costdef.displayProperties
        }
        item.costs.push(costsave);
    }

    //Identify if item is a bounty or not
    if (itemdef.inventory.bucketTypeHash == '1345459588') {
        item.bounty = true;
    } else {
        item.bounty = false;
    }

    //If item has perks, get display properties of all of them
    if (itemdef.sockets) {
        for (let socketcatkey in itemdef.sockets.socketCategories) {
            let socketcat = itemdef.sockets.socketCategories[socketcatkey]
            let socketcatdef = manifest.DestinySocketCategoryDefinition[socketcat.socketCategoryHash];
            if (socketcatdef.categoryStyle == 1) {
                item.sockets = [];
                if (itemsockets[saleitem.vendorItemIndex]) {
                    let sockets = itemsockets[saleitem.vendorItemIndex].sockets;
                    for (let socketindexkey in socketcat.socketIndexes) {
                        let socketindex = socketcat.socketIndexes[socketindexkey];
                        let socket = sockets[socketindex];
                        console.log(socket);
                        if (socket.isVisible) {
                            let socketsave = {
                                plugs: []
                            }
                            if (socket.reusablePlugHashes) {
                                for (let plugkey in socket.reusablePlugHashes) {
                                    let plughash = socket.reusablePlugHashes[plugkey];
                                    let plugdef = manifest.DestinyInventoryItemDefinition[plughash];
                                    socketsave.plugs.push(plugdef.displayProperties);
                                }
                            } else if (socket.plugHash) {
                                let plughash = socket.plugHash;
                                let plugdef = manifest.DestinyInventoryItemDefinition[plughash];
                                socketsave.plugs.push(plugdef.displayProperties);
                            }
                            if (socketsave.plugs.length == 0) {
                                console.log('BIG OOF');
                            }
                            item.sockets.push(socketsave);
                        }
                    }
                }
            }
        }
    }

    //If item is class locked, identity the class it's locked to
    if (itemdef.classType != undefined) {
        if (itemdef.classType == 0) {
            item.class = 'Titan';
        } else if (itemdef.classType == 1) {
            item.class = 'Hunter';
        } else if (itemdef.classType == 2) {
            item.class = 'Warlock';
        }
    }

    return item;
}

function isItXurTime() {
    let time = new Date();
    let day = time.getUTCDay();
    let hour = time.getUTCHours();

    if ((weeklyReset < day && day < xurDay) || (day == weeklyReset && hour >= morningReset) || (day == xurDay && hour < morningReset)) {
        // 2 is Tuesday, 5 is Friday.
        return false;
    }
    else {
        return true;
    }
}

const delay = ms => new Promise(resolve => setTimeout(resolve, ms));
